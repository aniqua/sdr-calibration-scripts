import contextlib
import socket
import sys
import time
import statistics
import os

from ldb.hdf5.high_level import HDF5File, HDF5Dataspace, HDF5TypeBuiltin

import alert_thread

import uhd
import numpy
import cmath
import math

print("\033];Input Power Sweep (VSG -> radio)\007")

sdr = "X310"
mode = 0 # 0 = debug without anything connected
         # 1 = connected with a Keysight signal generator
         # 2 = connected with a calibreated B210 signal generator

if sdr == "B210":
    frequency_start = 70e6
    default_frequency = 2.4e9
    ports = [(0, "TX/RX", "A TX/RX"),
             (0, "RX2", "A RX2"),
             (1, "TX/RX", "B TX/RX"),
             (1, "RX2", "B RX2")]
    n_frontends = 2
    powers = range(77)
    tt_powers = range(77)
    adc_power = 60
    rate = 30.72e6

elif sdr == "X310": #Note: might need to change some of the params
    frequency_start = 70e6
    default_frequency = 2.4e9
    ports = [(0, "TX/RX", "TX/RX"),
             (0, "RX2", "RX2")]
    n_frontends = 1
    powers = range(32)
    tt_powers = range(32)
    adc_power = 30
    rate = 28.5e6 #Q: ?

else:
    print("SDR = %s is not supported" %sdr)
    exit(1)




# Indexing in HDF5 will be:
# port with power, port receive stream, frequency, power
# Take some time series at different power levels (distortion)
# Take broader power range at a few ports

#frequencies = [10e6*i for i in range(1, 601)]
frequency_step = 100e6
frequencies = [frequency_start] + [frequency_step*i for i in range(1, 61)]
#frequencies = [70e6]
tt_frequencies = [frequency_start, 100e6, 200e6, 400e6, 800e6,
                  1e9, 2e9, 3e9, 4e9, 5e9, 6e9]
#tt_frequencies = [70e6]
adc_frequency = 2e9






portmap = {(port[0], port[1]): i for i, port in enumerate(ports)}


if_frequencies_ = ([0.1*i for i in range(9)] +
                   [0.01*i+0.9 for i in range(4)] +
                   [0.0025*i+0.94 for i in range(21)] +
                   [0.999, 1.0])
if_frequencies = [-if_freq for if_freq in if_frequencies_[-1:0:-1]] + \
  if_frequencies_


frequency_offset_cw = 10e3


try:
    device = uhd.usrp.MultiUSRP()
except RuntimeException as e:
    if e.message == "LookupError: KeyError: No devices found for ----->\nEmpty Device Address":
        print("... engage telepathy ... FAIL ... look for SDR ... FAIL")
        print("Trying plugging in the SDR")
        sys.exit(1)
    else:
        raise e

if device.get_mboard_name() != sdr:
    print("... This is not the radio you are looking for ... FAIL")
    print("Weakminded one")
    sys.exit(1)


mboard_serial_number = device.get_usrp_rx_info()["mboard_serial"]
rx_serial_number = device.get_usrp_rx_info()["rx_serial"]
serial_number = mboard_serial_number + "_" + rx_serial_number
print("Serial number: ", serial_number)

samp_rate_tt = rate
samp_rate_cw = samp_rate_tt/32

for i in range(2):
    device.set_rx_rate(samp_rate_tt, i)
    tune_req = uhd.types.TuneRequest(default_frequency)
    device.set_rx_freq(tune_req, i)
    device.set_rx_gain(76, i)

for i in range(2):
    device.set_tx_rate(samp_rate_tt, i)
    tune_req = uhd.types.TuneRequest(default_frequency)
    device.set_tx_freq(tune_req, i)
    device.set_tx_gain(70, i)

try:
    self.device.set_clock_source('external')
except:
    print("Failed to set clock to external")


n_tx = 4096
n_rx = 1024

n_rx_tt = 30720


stream_args = uhd.libpyuhd.usrp.stream_args("fc32", "sc16")
stream_args.channels = [0, 1]

rx_stream = device.get_rx_stream(stream_args)
#rxStreams_tt = [sdr.setupStream(SoapySDR.SOAPY_SDR_RX, SoapySDR.SOAPY_SDR_CF32,
#                                [0]),
#                sdr.setupStream(SoapySDR.SOAPY_SDR_RX, SoapySDR.SOAPY_SDR_CF32,
#                                [1])]

rx_buff = numpy.zeros((2, n_rx), dtype=numpy.complex64)
rx_tmp_buff = numpy.zeros((2, rx_stream.get_max_num_samps()),
                          dtype=numpy.complex64)
rx_buff_1 = numpy.array([0]*n_rx, numpy.complex64)
rx_buff_2 = numpy.array([0]*n_rx, numpy.complex64)

tt_rx_buff = numpy.zeros((2, n_rx_tt), dtype=numpy.complex64)
tt_rx_tmp_buff = numpy.zeros((2, rx_stream.get_max_num_samps()),
                             dtype=numpy.complex64)

metadata = uhd.libpyuhd.types.rx_metadata()

cw_stream_cmd = uhd.libpyuhd.types.stream_cmd(
    uhd.libpyuhd.types.stream_mode.num_done)
cw_stream_cmd.num_samps = n_rx
cw_stream_cmd.stream_now = False

tt_stream_cmd = uhd.libpyuhd.types.stream_cmd(
    uhd.libpyuhd.types.stream_mode.num_done)
tt_stream_cmd.num_samps = n_rx_tt
tt_stream_cmd.stream_now = False


if mode == 1:
    n5172b_ip = '192.168.90.132'
    port = 5025
else:
    frequency_start = 3586e6
    default_frequency = 3586e6
    frequency_step = 2e6
    frequencies = [frequency_start] + [frequency_start+frequency_step*i for i in range(1, 4)]
    subset_frequencies = frequencies
    powers = range(5)
    tt_powers = range(5)

out_folder = 'calibration_data/'
if not os.path.exists(out_folder):
    os.mkdir(out_folder)

with contextlib.ExitStack() as context_stack:
    h5_filename = out_folder + "in_%s.h5"%(serial_number)
    
    if os.path.exists(h5_filename):
        os.remove(h5_filename)

    h5_file = context_stack.enter_context(
        HDF5File.open(h5_filename))

    amplitude_dataspace = context_stack.enter_context(
        HDF5Dataspace.create_simple(
            [len(ports), len(ports), len(frequencies), len(powers)]))
    twotone_dataspace = context_stack.enter_context(
        HDF5Dataspace.create_simple(
            [len(ports), len(ports), len(tt_frequencies), len(tt_powers),
             2, n_rx_tt]))
    adc_amplitude_dataspace = context_stack.enter_context(
        HDF5Dataspace.create_simple(
            [len(ports), len(ports), len(if_frequencies)]))


    siggen_amplitude_dataset = context_stack.enter_context(
        h5_file.create_dataset("siggen_amplitude_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               amplitude_dataspace))
    amplitude_dataset = context_stack.enter_context(
        h5_file.create_dataset("amplitude_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               amplitude_dataspace))
    amplitude_dataset_stdev = context_stack.enter_context(
        h5_file.create_dataset("amplitude_dataset_stdev",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               amplitude_dataspace))

    siggen_amplitude_data = siggen_amplitude_dataset.make_data()
    amplitude_data = amplitude_dataset.make_data()
    amplitude_data_stdev = amplitude_dataset_stdev.make_data()

    twotone_waveform_dataset = context_stack.enter_context(
        h5_file.create_dataset("twotone_waveform_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               twotone_dataspace))

    twotone_waveform_data = twotone_waveform_dataset.make_data()

    adc_siggen_amplitude_dataset = context_stack.enter_context(
        h5_file.create_dataset("adc_siggen_amplitude_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               adc_amplitude_dataspace))
    adc_amplitude_dataset = context_stack.enter_context(
        h5_file.create_dataset("adc_amplitude_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               adc_amplitude_dataspace))
    adc_amplitude_dataset_stdev = context_stack.enter_context(
        h5_file.create_dataset("adc_amplitude_dataset_stdev",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               adc_amplitude_dataspace))

    adc_siggen_amplitude_data = adc_siggen_amplitude_dataset.make_data()
    adc_amplitude_data = adc_amplitude_dataset.make_data()
    adc_amplitude_data_stdev = adc_amplitude_dataset_stdev.make_data()

    if mode == 1:
        s = context_stack.enter_context(
            socket.socket(socket.AF_INET, socket.SOCK_STREAM))
        s.connect((n5172b_ip, port))
        s.sendall(b'*IDN?\n')

        result = s.recv(1024).decode('ascii')
        model = result.split(',')[1].strip()
        assert model == 'N5172B'

        s.sendall(b':CORR?\n')
        result = s.recv(1024).decode('ascii')
        if result != '1\n':
            print("Correction is not on, either load some values, or run the setup script")
            sys.exit(1)

        s.sendall(b':POW:USER:MAX?\n')
        result = s.recv(1024).decode('ascii')
        s.sendall(b':POW:USER:ENAB?\n')
        result2 = s.recv(1024).decode('ascii')
        if float(result) != -15 or int(result2) != 1:
            print("Max power not setup properly. Run the setup script.")
            sys.exit(1)

        s.sendall(b':OUTPUT:MOD 0\n')
        s.sendall(b':RAD:TTON:ARB:BAS:FREQ:OFFS 2.3MHz\n')
        s.sendall(b':RAD:TTON:ARB:FSP 1MHz\n')
        s.sendall(b':RAD:TTON:ARB:APPLY\n')
        s.sendall(b':RAD:TTON:ARB 1\n')


        result = 'n'
        while result != 'y':
            result = input('Signal generator ready? [y/n]: ')

    start = time.time()
    last = time.time()

    for channel, antenna, port_name in ports:
        for i in range(2):
            device.set_rx_rate(samp_rate_cw, i)

        input_result = 'n'
        print("Time since last: %f"%(time.time()-last))
        a_thread = alert_thread.AlertThread()
        a_thread.start()
        while input_result != 'y':
            if mode == 1:
                s.sendall(b':OUTPUT 0\n')
                input_result = input(
                    'Connect signal generator to port %s? [y/n]: '% (port_name))
            else:
                input_result = 'y'

            if input_result == 'y':
                if mode == 1:
                    s.sendall(b':OUTPUT 1\n')
                    s.sendall(b':OUTPUT:MOD 0\n')
                tune_req = uhd.types.TuneRequest(frequency_start)
                for i in range(2):
                    device.set_rx_freq(tune_req, i)
                if mode == 1:
                    s.sendall(b':FREQ %f\n'%(frequency_start-frequency_offset_cw))

                wave = [cmath.exp(2j*math.pi*i*frequency_offset_cw/samp_rate_cw)
                        for i in range(len(rx_buff_1))]

                pow_ = 0
                siggen_power = (-pow_-15)
                if mode == 1:
                    s.sendall(b':POW %f\n'%(siggen_power))

                    while True:
                        s.sendall(b'*OPC?\n')
                        result = s.recv(1024).decode('ascii')
                        if int(result) == 1:
                            break

                for i in range(2):
                    device.set_rx_gain(pow_, i)

                for i in range(2):
                    device.set_rx_antenna(antenna, i)

                sdr_time = device.get_time_now().get_real_secs()
                j = 0
                cw_stream_cmd.time_spec = \
                        uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)
                rx_stream.issue_stream_cmd(cw_stream_cmd)
                while j < n_rx:
                    read_count = rx_stream.recv(rx_tmp_buff, metadata)
                    rx_buff[:, j:j+read_count] = rx_tmp_buff[:,
                                                             0:read_count]
                    j += read_count

                sdr_time = device.get_time_now().get_real_secs()
                j = 0
                cw_stream_cmd.time_spec = \
                        uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)
                rx_stream.issue_stream_cmd(cw_stream_cmd)
                while j < n_rx:
                    read_count = rx_stream.recv(rx_tmp_buff, metadata)
                    rx_buff[:, j:j+read_count] = \
                            rx_tmp_buff[:, 0:read_count]
                    j += read_count
                    #print(rx_buff[:, 0:200], wave[0:200])

                magnitude = (sum([(a*b)
                                  for a,b in zip(rx_buff[channel, :],
                                                 wave)])/
                             len(wave))
                if magnitude == 0:
                    db_magnitude = -300
                else:
                    db_magnitude = 20*math.log10(abs(magnitude))

                print(db_magnitude)

                if db_magnitude < -30:
                    print("...this isn't telepathy, plug it in and turn it on...")
                    input_result = 'n'

                if mode == 0:
                    input_result = 'y'


        a_thread.running = False
        a_thread.join()



        last = time.time()
        if mode == 1:
            s.sendall(b':OUTPUT 1\n')
            s.sendall(b':OUTPUT:MOD 0\n')

        # 10 kHz wave
        wave = [cmath.exp(2j*math.pi*i*frequency_offset_cw/samp_rate_cw)
                for i in range(len(rx_buff_1))]
        for frequency_idx, freq in enumerate(frequencies):
            print(freq)
            if mode == 1:
                s.sendall(b':FREQ %f\n'%(freq-frequency_offset_cw))

                while True:
                    s.sendall(b'*OPC?\n')
                    result = s.recv(1024).decode('ascii')
                    if int(result) == 1:
                        break

            tune_req = uhd.types.TuneRequest(freq)
            for i in range(2):
                device.set_rx_freq(tune_req, i)

            for power_idx, pow_ in enumerate(powers):
                if pow_ % 10 == 0:
                    siggen_power = (-pow_-15)
                    if mode == 1:
                        s.sendall(b':POW %f\n'%(siggen_power))

                        while True:
                            s.sendall(b'*OPC?\n')
                            result = s.recv(1024).decode('ascii')
                            if int(result) == 1:
                                break

                for i in range(2):
                    device.set_rx_gain(pow_, i)

                for in_antenna in ["TX/RX", "RX2"]:
                    results = [[], []]
                    for i in range(2):
                        device.set_rx_antenna(in_antenna, i)

                    # TODO: First datapoint includes old data. Flush it out
                    # after any settings changes.
                    # This contains trash data, used to flush it out.
                    sdr_time = device.get_time_now().get_real_secs()
                    j = 0
                    cw_stream_cmd.time_spec = \
                            uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)
                    rx_stream.issue_stream_cmd(cw_stream_cmd)
                    while j < n_rx:
                        read_count = rx_stream.recv(rx_tmp_buff, metadata)
                        rx_buff[:, j:j+read_count] = rx_tmp_buff[:,
                                                                 0:read_count]
                        j += read_count

                    for i in range(10):
                        sdr_time = device.get_time_now().get_real_secs()
                        j = 0
                        cw_stream_cmd.time_spec = \
                                uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)
                        rx_stream.issue_stream_cmd(cw_stream_cmd)
                        while j < n_rx:
                            read_count = rx_stream.recv(rx_tmp_buff, metadata)
                            rx_buff[:, j:j+read_count] = \
                                    rx_tmp_buff[:, 0:read_count]
                            j += read_count
                            #print(rx_buff[:, 0:200], wave[0:200])
                        for j in range(2):
                            magnitude = (sum([(a*b)
                                              for a,b in zip(rx_buff[j, :],
                                                             wave)])/
                                         len(wave))
                            if magnitude == 0:
                                db_magnitude = -300
                            else:
                                db_magnitude = 20*math.log10(abs(magnitude))
                            # TODO: Undo lazy, need to work out statistics of
                            # looking at average and the noise on this, etc
                            results[j].append(db_magnitude)
                    # TODO: Handle different log levels
                    if False:
                        print(results)
                    for j in range(n_frontends):
                        print(j, in_antenna, pow_, statistics.mean(results[j]),
                              statistics.stdev(results[j]))
                        dataset_idx = (portmap[(channel, antenna)],
                                       portmap[(j, in_antenna)],
                                       frequency_idx, power_idx)
                        amplitude_data[dataset_idx] = \
                                statistics.mean(results[j])
                        amplitude_data_stdev[dataset_idx] = \
                                statistics.stdev(results[j])
                        siggen_amplitude_data[dataset_idx] = \
                                siggen_power

            amplitude_dataset[...] = amplitude_data
            siggen_amplitude_dataset[...] = siggen_amplitude_data
            amplitude_dataset_stdev[...] = amplitude_data_stdev

        if mode == 1:
            s.sendall(b':OUTPUT:MOD 1\n')
        for i in range(2):
            device.set_rx_rate(samp_rate_tt, i)

        for frequency_idx, freq in enumerate(tt_frequencies):
            print(freq)
            if mode == 1:
                s.sendall(b':FREQ %f\n'%(freq-frequency_offset_cw))

                while True:
                    s.sendall(b'*OPC?\n')
                    result = s.recv(1024).decode('ascii')
                    if int(result) == 1:
                        break

            tune_req = uhd.types.TuneRequest(freq)
            for i in range(2):
                device.set_rx_freq(tune_req, i)

            for power_idx, pow_ in enumerate(tt_powers):
                siggen_power = (-pow_-15)
                if mode == 1:
                    s.sendall(b':POW %f\n'%(siggen_power))

                    while True:
                        s.sendall(b'*OPC?\n')
                        result = s.recv(1024).decode('ascii')
                        if int(result) == 1:
                            break

                for i in range(2):
                    device.set_rx_gain(pow_, i)

                for in_antenna in ["TX/RX", "RX2"]:
                    results = [[], []]
                    for i in range(2):
                        device.set_rx_antenna(in_antenna, i)

                    # TODO: First datapoint includes old data. Flush it out
                    # after any settings changes.
                    # This contains trash data, used to flush it out.
                    sdr_time = device.get_time_now().get_real_secs()
                    j = 0
                    cw_stream_cmd.time_spec = \
                            uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)
                    rx_stream.issue_stream_cmd(cw_stream_cmd)
                    while j < n_rx:
                        read_count = rx_stream.recv(rx_tmp_buff, metadata)
                        rx_buff[:, j:j+read_count] = rx_tmp_buff[:,
                                                                 0:read_count]
                        j += read_count

                    sdr_time = device.get_time_now().get_real_secs()
                    j = 0
                    tt_stream_cmd.time_spec = \
                            uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)
                    rx_stream.issue_stream_cmd(tt_stream_cmd)
                    while j < n_rx_tt:
                        read_count = rx_stream.recv(tt_rx_tmp_buff, metadata)
                        tt_rx_buff[:, j:j+read_count] = \
                                tt_rx_tmp_buff[:, 0:read_count]
                        j += read_count


                    # TODO: Stuff in data array
                    for j in range(n_frontends):
                        dataset_idx_re = (channel, portmap[(j, in_antenna)],
                                          frequency_idx, power_idx, 0,
                                          slice(0,n_rx_tt))
                        dataset_idx_im = (channel, portmap[(j, in_antenna)],
                                          frequency_idx, power_idx, 1,
                                          slice(0,n_rx_tt))
                        twotone_waveform_data[dataset_idx_re] = \
                                tt_rx_buff[j, :].real
                        twotone_waveform_data[dataset_idx_im] = \
                                tt_rx_buff[j, :].imag
                    print(in_antenna, pow_)

            twotone_waveform_dataset[...] = twotone_waveform_data

        if mode == 1:
            s.sendall(b':OUTPUT:MOD 0\n')
        for if_frequency_idx, if_frequency in enumerate(if_frequencies):
            if_frequency *= samp_rate_tt/2

            adc_wave = [cmath.exp(2j*math.pi*i*if_frequency/samp_rate_tt)
                        for i in range(n_rx_tt)]

            if mode == 1:
                s.sendall(b':FREQ %f\n'%(adc_frequency-if_frequency))

            siggen_power = (-adc_power-15)
            if mode == 1:
                s.sendall(b':POW %f\n'%(siggen_power))
                while True:
                    s.sendall(b'*OPC?\n')
                    result = s.recv(1024).decode('ascii')
                    if int(result) == 1:
                        break

            for i in range(2):
                device.set_rx_gain(adc_power, i)

            tune_req = uhd.types.TuneRequest(adc_frequency)
            for i in range(2):
                device.set_rx_freq(tune_req, i)

            for in_antenna in ["TX/RX", "RX2"]:
                results = [[], []]

                for i in range(2):
                    device.set_rx_antenna(in_antenna, i)

                # TODO: First datapoint includes old data. Flush it out
                # after any settings changes.
                # This contains trash data, used to flush it out.
                sdr_time = device.get_time_now().get_real_secs()
                j = 0
                cw_stream_cmd.time_spec = \
                        uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)
                rx_stream.issue_stream_cmd(cw_stream_cmd)
                while j < n_rx:
                    read_count = rx_stream.recv(rx_tmp_buff, metadata)
                    rx_buff[:, j:j+read_count] = rx_tmp_buff[:,
                                                             0:read_count]
                    j += read_count

                for i in range(10):
                    sdr_time = device.get_time_now().get_real_secs()
                    j = 0
                    tt_stream_cmd.time_spec = \
                            uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)
                    rx_stream.issue_stream_cmd(tt_stream_cmd)
                    while j < n_rx_tt:
                        read_count = rx_stream.recv(tt_rx_tmp_buff, metadata)
                        tt_rx_buff[:, j:j+read_count] = \
                                tt_rx_tmp_buff[:, 0:read_count]
                        j += read_count


                    for j in range(2):
                        magnitude = (sum([(a*b)
                                          for a,b in zip(tt_rx_buff[j, :],
                                                         adc_wave)])/
                                     len(adc_wave))
                        if magnitude == 0:
                            db_magnitude = -300
                        else:
                            db_magnitude = 20*math.log10(abs(magnitude))
                        # TODO: Undo lazy, need to work out statistics of
                        # looking at average and the noise on this, etc
                        results[j].append(db_magnitude)
                for j in range(n_frontends):
                    print(j, in_antenna, if_frequency,
                          statistics.mean(results[j]),
                          statistics.stdev(results[j]))
                    dataset_idx = (portmap[channel, antenna],
                                   portmap[(j, in_antenna)],
                                   if_frequency_idx)
                    adc_amplitude_data[dataset_idx] = \
                            statistics.mean(results[j])
                    adc_amplitude_data_stdev[dataset_idx] = \
                            statistics.stdev(results[j])
                    adc_siggen_amplitude_data[dataset_idx] = \
                            siggen_power

            adc_amplitude_dataset[...] = adc_amplitude_data
            adc_siggen_amplitude_dataset[...] = adc_siggen_amplitude_data
            adc_amplitude_dataset_stdev[...] = adc_amplitude_data_stdev

    print(time.time() - start)
