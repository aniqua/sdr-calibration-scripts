import contextlib
import socket
import sys
import time
import threading
import statistics
import os

import cmath
import math

from ldb.hdf5.high_level import HDF5File, HDF5Dataspace, HDF5TypeBuiltin

import alert_thread

import uhd

import numpy


class GeneratorThread(threading.Thread):
	def __init__(self):
		super().__init__()
		try:
			self.device = uhd.usrp.MultiUSRP()
		except RuntimeException as e:
			if e.message == "LookupError: KeyError: No devices found for ----->\nEmpty Device Address":
				print("... engage telepathy ... FAIL ... look for SDR ... FAIL")
				print("Trying plugging in the SDR")
				sys.exit(1)
			else:
				raise e

		if self.device.get_mboard_name() != sdr:
			print("... This is not the radio you are looking for ... FAIL")
			print("Weakminded one")
			sys.exit(1)

		for i in range(2):
			self.device.set_tx_rate(rate, i)
			self.device.set_rx_rate(rate, i)
			self.device.set_rx_antenna("RX2", i)

		try:
			self.device.set_clock_source('external')
		except:
			print("Failed to set clock to external")


		self.mboard_serial_number = \
				self.device.get_usrp_rx_info()["mboard_serial"]
		self.rx_serial_number = self.device.get_usrp_rx_info()["rx_serial"]
		print("Motherboard serial number: ", self.mboard_serial_number)
		print("RX serial number: ", self.rx_serial_number)

		stream_args = uhd.libpyuhd.usrp.stream_args("fc32", "sc16")
		stream_args.channels = [0, 1]

		self.tx_stream = self.device.get_tx_stream(stream_args)
		self.rx_stream = self.device.get_rx_stream(stream_args)

		self.n_rx = 4000
		self.n_tx = 4000

		self.tx_buff = numpy.array([[0]*self.n_tx, [0]*self.n_tx], dtype=numpy.complex64)
		self.rx_buff = numpy.array([[0]*self.n_rx, [0]*self.n_rx],
								   dtype=numpy.complex64)
		self.tmp_rx_buff = numpy.array(
			[[0]*self.rx_stream.get_max_num_samps(),
			 [0]*self.rx_stream.get_max_num_samps()],
			dtype=numpy.complex64)

		self.event_flush = threading.Event()
		self.event_flush_done = threading.Event()
		self.if_frequency = 0
		self.i_amplitude = 0
		self.q_amplitude = 0
		self.channel = 0

		self.gain = 0

		self._shutdown = False

	def run(self):
		tx_stream = self.tx_stream
		metadata = uhd.libpyuhd.types.tx_metadata()
		async_metadata = uhd.libpyuhd.types.async_metadata()
		while True:
			while not self.event_flush.is_set():
				tx_stream.send(self.tx_buff, metadata)
			if self._shutdown:
				break
			self.event_flush.clear()
			while True:
				if not tx_stream.recv_async_msg(async_metadata):
					continue
				if async_metadata.event_code == \
				   uhd.libpyuhd.types.tx_metadata_event_code.underflow:
					break
			self.event_flush_done.set()

	def __enter__(self):
		self.start()
		return self

	def __exit__(self, exc_type, exc_value, traceback):
		self.shutdown()

	def shutdown(self):
		self._shutdown = True
		self.event_flush.set()

	def update_stream(self):
		# if_frequency rel to Nyquist frequency
		data = [self.i_amplitude*math.cos(math.pi*i*self.if_frequency)+
				self.q_amplitude*math.sin(math.pi*i*self.if_frequency)*1j
				for i in range(self.n_tx)]
		self.tx_buff = numpy.array([data if self.channel == 0 else [0]*self.n_tx,
									data if self.channel == 1 else [0]*self.n_tx],
								   dtype=numpy.complex64)

		self.event_flush.set()
		self.event_flush_done.wait()
		self.event_flush_done.clear()

	def get_serial(self):
		return self.mboard_serial_number + "_" + self.rx_serial_number

	def set_rf_frequency(self, frequency):
		tune_request = uhd.types.TuneRequest(frequency)
		for i in range(2):
			self.device.set_rx_freq(tune_request, i)
			self.device.set_tx_freq(tune_request, i)

	def set_if_frequency(self, frequency):
		self.if_frequency = frequency

		self.update_stream()

	def set_gain(self, gain):
		self.gain = gain
		for i in range(2):
			self.device.set_tx_gain(gain, i)
			self.device.set_rx_gain(min(int(110-gain), 76), i)

	def set_amplitude(self, i_amplitude, q_amplitude):
		self.i_amplitude = i_amplitude
		self.q_amplitude = q_amplitude

		self.update_stream()

	def set_channel(self, channel):
		self.channel = channel

		self.update_stream()

	def get_receive_levels(self):
		metadata = uhd.libpyuhd.types.rx_metadata()
		stream_cmd = uhd.libpyuhd.types.stream_cmd(
			uhd.libpyuhd.types.stream_mode.num_done)
		stream_cmd.num_samps = self.n_rx
		stream_cmd.stream_now = False

		results = [[]]*n_frontends #[[], []] #Q: Why 2??
		for i in range(11):
			sdr_time = self.device.get_time_now().get_real_secs()
			stream_cmd.time_spec = \
					uhd.libpyuhd.types.time_spec(sdr_time + 4e-3)

			self.rx_stream.issue_stream_cmd(stream_cmd)

			j = 0

			while j < self.n_rx:
				read_count = self.rx_stream.recv(self.tmp_rx_buff, metadata)
				self.rx_buff[:, j:j+read_count] = \
						self.tmp_rx_buff[:, 0:read_count]
				j += read_count

			for j in range(n_frontends):
				if False:
					magnitude = (sum([(a*b.conjugate())
									  for a,b in zip(self.rx_buff[j, :],
													 self.tx_buff[self.channel,
																  :])])/
								 self.n_rx)
				magnitude = numpy.dot(self.rx_buff[j, :].conjugate(),
									  self.tx_buff[self.channel, :]) / \
						self.n_rx
				if magnitude == 0:
					db_magnitude = -300
				else:
					db_magnitude = 20*math.log10(abs(magnitude))

				results[j].append(db_magnitude)

		stats = [[statistics.mean(results[i][1:]),
				  statistics.stdev(results[i][1:])]
				 for i in range(2)]

		return stats


# Sweeps:
# * if_frequency to understand if filter roll off and any DAC rolloff
# * rf_frequency to calibrate output power to gain setting
# * amplitude to find P1dB
# * i,q channels separately to calibrate i/q imbalance
# * tx_gain to calibrate accuracy of db scale
# * at least rf_frequency x tx_gain to measure flatness of tx_gain parts
# * i,q x rf_frequency should be low-cost to do
# * amplitude x rf_frequency to plot flatness of P1dB
# * sporadic if_frequency x rf_frequency, don't expect interaction
# * if_frequency x amplitude to catch anything like maximum slope effects

# for each rf_frequency
#   * sweep subset of tx_gain (careful pick to try to pickup single attenuator
#       switches)
#   * i,q imbalance
#   * sweep amplitude
#   * sweep subset of if_frequency
# at a few rf_frequencies
#   * sweep subset of if_frequency x amplitude
#   * sweep full if_frequency
#   * sweep larger set of tx_gain
# at one rf_frequency
#   * sweep full set of tx_gain

# Look at leakage on sweep of tx_gain
# Look at leakage with i,q imbalance
# Look at leakage with amplitude
# Look at leakage with if_frequency

sdr = "X310"
mode = 0 # 0 = debug without anything connected
         # 1 = power measurement with a Keysight power meter
         # 2 = power measurement with a calibreated B210



if sdr == "B210":
	frequency_start = 70e6
	default_frequency = 2.4e9

	ports = [(0, "TX/RX", "A TX/RX"),
			 (1, "TX/RX", "B TX/RX")]

	gain_subset = [0, 10, 20, 30, 40, 50, 60, 70] + \
		[70+0.25*i for i in range(1, 80)]
	gain_more_subset = [i for i in range(89)] + [89.75]
	gain_all = [0.25*i for i in range(360)]
	default_gain = 89.75
	test_gain = 89

	rate = 30.72e6
	n_frontends = 2 # Q: why is that?

elif sdr == "X310": #Note: might need to change these
	frequency_start = 70e6
	default_frequency = 2.4e9

	ports = [(0, "TX/RX", "TX/RX")]

	gain_subset = [0, 10, 20] + \
				  [20+0.5*i for i in range(1, 23)]
	gain_more_subset = [i for i in range(32)] + [31.5]
	gain_all = [0.5*i for i in range(64)]
	default_gain = 31.5
	test_gain = 31
	
	rate = 28.5e6
	n_frontends = 2 # Q: should be 1?


else:
	print("SDR = %s is not supported" %sdr)
	exit(1)


portmap = {(port[0], port[1]): i for i, port in enumerate(ports)}



frequency_step = 100e6
frequencies = [frequency_start] + [frequency_step*i for i in range(1, 61)]
subset_frequencies = [frequency_start, 100e6, 200e6, 400e6, 800e6,
					  1e9, 2e9, 3e9, 4e9, 5e9, 6e9]


if_frequencies_ = ([0.1*i for i in range(9)] +
				   [0.01*i+0.9 for i in range(4)] +
				   [0.0025*i+0.94 for i in range(21)] +
				   [0.999, 1.0])


default_frequency = 2.4e9
if_frequencies = [-if_freq for if_freq in if_frequencies_[-1:0:-1]] + \
  if_frequencies_
default_if_frequency = 0.01



amplitudes_db = [i/10 for i in range(-200,1)]
amplitudes = [10**(amplitude_db/20) for amplitude_db in amplitudes_db]
default_amplitude = 1.0

if_frequencies_subset_ = [0, 0.1, 0.9, 0.99, 0.999, 1.0]
if_frequencies_subset = [-if_freq
						 for if_freq in if_frequencies_subset_[-1:0:-1]] + \
  if_frequencies_subset_


if mode == 1:
	n1913a_ip = '192.168.90.129'
	port = 5025
else:
	frequency_start = 3586e6
	default_frequency = 3586e6
	frequency_step = 2e6
	frequencies = [frequency_start] + [frequency_start+frequency_step*i for i in range(1, 4)]
	subset_frequencies = frequencies

	gain_subset = [0, 10, 20, 30]
	gain_more_subset = [0, 10, 20, 30]
	gain_all = [0, 10, 20, 30]
	default_gain = 31
	test_gain = 31



out_folder = 'calibration_data/'
if not os.path.exists(out_folder):
	os.mkdir(out_folder)


with contextlib.ExitStack() as context_stack:
	
	generator = context_stack.enter_context(GeneratorThread())

	generator.set_gain(default_gain)
	generator.set_rf_frequency(default_frequency)
	generator.set_amplitude(1, 1)
	generator.set_if_frequency(0.1)

	h5_filename = out_folder + "out_%s.h5"%(generator.get_serial())
	
	if os.path.exists(h5_filename):
		os.remove(h5_filename)

	h5_file = context_stack.enter_context(
		HDF5File.open(h5_filename))
	
	gain_subset_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), len(frequencies), len(gain_subset)]))
	
	
	iq_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), len(frequencies), 2]))
	amplitude_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), len(frequencies), len(amplitudes)]))
	if_subset_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), len(frequencies), len(if_frequencies_subset)]))
	leak_gain_subset_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), n_frontends, len(frequencies), len(gain_subset)]))
	leak_iq_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), n_frontends, len(frequencies), 2]))
	leak_amplitude_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), n_frontends, len(frequencies), len(amplitudes)]))
	leak_if_subset_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), n_frontends, len(frequencies),
			 len(if_frequencies_subset)]))
	if_amplitude_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), len(subset_frequencies), len(if_frequencies_subset),
			 len(amplitudes)]))
	if_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), len(subset_frequencies), len(if_frequencies)]))
	gain_more_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), len(subset_frequencies), len(gain_more_subset)]))
	gain_all_sweep_dataspace = context_stack.enter_context(
		HDF5Dataspace.create_simple(
			[len(ports), len(gain_all)]))

	gain_subset_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("gain_subset_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   gain_subset_sweep_dataspace))
	iq_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("iq_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   iq_sweep_dataspace))
	amplitude_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("amplitude_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   amplitude_sweep_dataspace))
	if_subset_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("if_subset_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   if_subset_sweep_dataspace))
	if_amplitude_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("if_amplitude_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   if_amplitude_sweep_dataspace))
	if_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("if_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   if_sweep_dataspace))
	gain_more_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("gain_more_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   gain_more_sweep_dataspace))
	gain_all_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("gain_all_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   gain_all_sweep_dataspace))

	leak_amp_gain_subset_sweep_dataset = context_stack.enter_context(
				 h5_file.create_dataset("leak_amp_gain_subset_sweep_dataset",
										HDF5TypeBuiltin.IEEE_F64LE,
										leak_gain_subset_sweep_dataspace))
	leak_amp_iq_sweep_dataset = context_stack.enter_context(
				 h5_file.create_dataset("leak_amp_iq_sweep_dataset",
										HDF5TypeBuiltin.IEEE_F64LE,
										leak_iq_sweep_dataspace))
	leak_amp_amplitude_sweep_dataset = context_stack.enter_context(
				 h5_file.create_dataset("leak_amp_amplitude_sweep_dataset",
										HDF5TypeBuiltin.IEEE_F64LE,
										leak_amplitude_sweep_dataspace))
	leak_amp_if_subset_sweep_dataset = context_stack.enter_context(
				 h5_file.create_dataset("leak_amp_if_subset_sweep_dataset",
										HDF5TypeBuiltin.IEEE_F64LE,
										leak_if_subset_sweep_dataspace))

	leak_stdev_gain_subset_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("leak_stdev_gain_subset_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   leak_gain_subset_sweep_dataspace))
	leak_stdev_iq_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("leak_stdev_iq_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   leak_iq_sweep_dataspace))
	leak_stdev_amplitude_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("leak_stdev_amplitude_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   leak_amplitude_sweep_dataspace))
	leak_stdev_if_subset_sweep_dataset = context_stack.enter_context(
		h5_file.create_dataset("leak_stdev_if_subset_sweep_dataset",
							   HDF5TypeBuiltin.IEEE_F64LE,
							   leak_if_subset_sweep_dataspace))

	if mode == 1:
		s = context_stack.enter_context(
			socket.socket(socket.AF_INET, socket.SOCK_STREAM))
		s.connect((n1913a_ip, port))
		s.sendall(b'*IDN?\n')

		result = s.recv(1024).decode('ascii')
		model = result.split(',')[1].strip()
		assert model == 'N1913A'

		s.sendall(b'*RST\n')

		result = 'n'
		while result != 'y':
			result = input('Power meter ready? [y/n]: ')

	start = time.time()
	last = time.time()

	for channel, antenna, port_name in ports:
		if mode == 1:
			generator.set_channel(channel)
			input_result = 'n'
			print("Time since last: %f"%(time.time()-last))
			a_thread = alert_thread.AlertThread()
			a_thread.start()
			while input_result != 'y':
				input_result = input('Connect power meter to port %s? [y/n]: '%
							   (port_name))

				if input_result == 'y':
					freq = frequency_start
					generator.set_rf_frequency(freq)
					s.sendall(b'SENS3:FREQ %f\n'%(freq,))
					generator.set_if_frequency(default_if_frequency)
					generator.set_amplitude(default_amplitude, default_amplitude)
					generator.set_gain(test_gain)
					s.sendall(b'MEAS3?\n')
					result = s.recv(1024).decode('ascii')
					if float(result) < 0:
						print("...this isn't telepathy, plug it in and turn it on...")
						input_result = 'n'

			a_thread.running = False
			a_thread.join()

		print(portmap[(channel, antenna)])
		last = time.time()

		for frequency_idx, freq in enumerate(frequencies):
			print(freq)
			generator.set_rf_frequency(freq)
			if mode == 1:
				s.sendall(b'SENS3:FREQ %f\n'%(freq,))

			generator.set_if_frequency(default_if_frequency)
			generator.set_amplitude(default_amplitude, default_amplitude)

			for gain_idx, gain in enumerate(gain_subset):
				generator.set_gain(gain)
				if mode == 1:
					s.sendall(b'MEAS3?\n')
					result = s.recv(1024).decode('ascii')
				else:
					result = 1.0
				
				gain_subset_sweep_dataset[portmap[(channel, antenna)],
										  frequency_idx,
										  gain_idx] = float(result)
				
				print(gain, result)
				recv_levels = generator.get_receive_levels()
				print(recv_levels)
				for k in range(n_frontends):
					
					leak_amp_gain_subset_sweep_dataset[
						portmap[(channel, antenna)], k,
						frequency_idx, gain_idx] = recv_levels[k][0]

					
					leak_stdev_gain_subset_sweep_dataset[
						portmap[(channel, antenna)], k,
						frequency_idx, gain_idx] = recv_levels[k][1]

			

			
			generator.set_if_frequency(default_if_frequency)
			generator.set_gain(default_gain)

			generator.set_amplitude(1.0, 0)

			if mode == 1:
				s.sendall(b'MEAS3?\n')
				result = s.recv(1024).decode('ascii')
			else:
				result = 1.0 # Added for debug
			iq_sweep_dataset[portmap[(channel, antenna)],
							 frequency_idx, 0] = float(result) 
			print(result)
			recv_levels = generator.get_receive_levels()
			print(recv_levels)
			for k in range(n_frontends):
				leak_amp_iq_sweep_dataset[
					portmap[(channel, antenna)], k,
					frequency_idx, 0] = recv_levels[k][0]
				leak_stdev_iq_sweep_dataset[
					portmap[(channel, antenna)], k,
					frequency_idx, 0] = recv_levels[k][1]

			generator.set_amplitude(0, 1.0)
			if mode == 1:
				s.sendall(b'MEAS3?\n')
				result = s.recv(1024).decode('ascii')
			else:
				result = 1.0 # Added for debug
			iq_sweep_dataset[portmap[(channel, antenna)],
							 frequency_idx, 1] = float(result)
			print(result)
			recv_levels = generator.get_receive_levels()
			print(recv_levels)
			for k in range(n_frontends):
				leak_amp_iq_sweep_dataset[
					portmap[(channel, antenna)], k,
					frequency_idx, 1] = recv_levels[k][0]
				leak_stdev_iq_sweep_dataset[
					portmap[(channel, antenna)], k,
					frequency_idx, 1] = recv_levels[k][1]

			generator.set_gain(default_gain)

			for amplitude_idx, amplitude in enumerate(amplitudes):
				generator.set_amplitude(amplitude, amplitude)
				if mode == 1:
					s.sendall(b'MEAS3?\n')
					result = s.recv(1024).decode('ascii')
				else:
					result = 1.0 # Added for debug
				amplitude_sweep_dataset[portmap[(channel, antenna)],
										frequency_idx,
										amplitude_idx] = float(result)
				print(amplitude, result)
				recv_levels = generator.get_receive_levels()
				print(recv_levels)
				for k in range(n_frontends):
					leak_amp_amplitude_sweep_dataset[
						portmap[(channel, antenna)], k,
						frequency_idx, amplitude_idx] = recv_levels[k][0]
					leak_stdev_amplitude_sweep_dataset[
						portmap[(channel, antenna)], k,
						frequency_idx, amplitude_idx] = recv_levels[k][1]

			generator.set_amplitude(default_amplitude, default_amplitude)

			for if_frequency_idx, if_frequency in \
					enumerate(if_frequencies_subset):
				generator.set_if_frequency(if_frequency)
				if mode == 1:
					s.sendall(b'MEAS3?\n')
					result = s.recv(1024).decode('ascii')
				else:
					result = 1.0 # Added for debug
				if_subset_sweep_dataset[portmap[(channel, antenna)],
										frequency_idx,
										if_frequency_idx] = float(result)
				print(if_frequency, result)
				recv_levels = generator.get_receive_levels()
				print(recv_levels)
				for k in range(n_frontends):
					leak_amp_if_subset_sweep_dataset[
						portmap[(channel, antenna)], k,
						frequency_idx, if_frequency_idx] = recv_levels[k][0]
					leak_stdev_if_subset_sweep_dataset[
						portmap[(channel, antenna)], k,
						frequency_idx, if_frequency_idx] = recv_levels[k][1]

		for frequency_idx, freq in enumerate(subset_frequencies):
			print(freq)
			generator.set_rf_frequency(freq)
			if mode == 1:
				s.sendall(b'SENS3:FREQ %f\n'%(freq,))

			generator.set_gain(default_gain)

			for if_frequency_idx, if_frequency in \
					enumerate(if_frequencies_subset):
				generator.set_if_frequency(if_frequency)
				for amplitude_idx, amplitude in enumerate(amplitudes):
					generator.set_amplitude(amplitude, amplitude)
					if mode == 1:
						s.sendall(b'MEAS3?\n')
						result = s.recv(1024).decode('ascii')
					else:
						result = 1.0 # Added for debug
					if_amplitude_sweep_dataset[portmap[(channel, antenna)],
											   frequency_idx,
											   if_frequency_idx,
											   amplitude_idx] = float(result)
					print(if_frequency, amplitude, result)

			generator.set_amplitude(default_amplitude, default_amplitude)

			for if_frequency_idx, if_frequency in \
					enumerate(if_frequencies):
				generator.set_if_frequency(if_frequency)
				if mode == 1:
					s.sendall(b'MEAS3?\n')
					result = s.recv(1024).decode('ascii')
				else:
					result = 1.0 # Added for debug
				if_sweep_dataset[portmap[(channel, antenna)],
								 frequency_idx,
								 if_frequency_idx] = float(result)
				print(if_frequency, result)

			generator.set_if_frequency(default_if_frequency)
			generator.set_amplitude(default_amplitude, default_amplitude)

			for gain_idx, gain in enumerate(gain_more_subset):
				generator.set_gain(gain)
				if mode == 1:
					s.sendall(b'MEAS3?\n')
					result = s.recv(1024).decode('ascii')
				else:
					result = 1.0 # Added for debug
				gain_more_sweep_dataset[portmap[(channel, antenna)],
										frequency_idx,
										gain_idx] = float(result)
				print(gain, result)

		for gain_idx, gain in enumerate(gain_all):
			generator.set_gain(gain)
			if mode == 1:
				s.sendall(b'MEAS3?\n')
				result = s.recv(1024).decode('ascii')
			else:
				result = 1.0 # Added for debug
			gain_all_sweep_dataset[portmap[(channel, antenna)],
								   gain_idx] = float(result)
			print(gain, result)
