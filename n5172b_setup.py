import socket
import sys

n5172b_ip = '192.168.90.132'
port = 5025

print(
"""This program should be run at the start of the day and if the instrument is
reset. Is this currently the case [y/n]?"""
)

result = input("")
if result != 'y':
    sys.exit(1)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((n5172b_ip, port))
    s.sendall(b'*IDN?\n')

    result = s.recv(1024).decode('ascii')
    model = result.split(',')[1].strip()
    assert model == 'N5172B'

    result = 'n'
    while result != 'y':
        result = input('Connect power meter to VSG and hit y: ')

    s.sendall(b'*RST\n')

    s.sendall(b':CORR:FLAT:STEP:POIN 600\n')
    s.sendall(b':CORR:FLAT:STEP:STAR 10e6\n')
    s.sendall(b':CORR:FLAT:STEP:STOP 6e9\n')
    s.sendall(b':CORR:FLAT:INIT:FST\n')

    s.sendall(b':CORR:PMET:COMM:TYPE USB\n')
    s.sendall(b':CORR:PMET:COMM:USB:DEV "U2044XA,MY57250021"\n')

    s.sendall(b':POW -30\n')
    s.sendall(b':POW:USER:MAX -15\n')
    s.sendall(b':POW:USER:ENAB 1\n')

    result = 'n'
    while result != 'y':
        print('Hit local, go to AMPTD->More->User Flatness->Do Cal')
        result = input('Once done, hit y: ')


    s.sendall(b':CORR 1\n')

